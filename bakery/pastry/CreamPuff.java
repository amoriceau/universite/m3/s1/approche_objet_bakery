package bakery.pastry;

/**
 * The type Cream puff.
 */
public class CreamPuff extends DecoratorCake {

    /**
     * Instantiates a new Cream puff.
     */
    public CreamPuff() {
        super("Cream Puff");
        description = "Cream Puff";
    }
    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public double cost() {
        return 3.5;
    }
}
