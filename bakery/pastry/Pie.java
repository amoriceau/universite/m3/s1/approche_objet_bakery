package bakery.pastry;

/**
 * The type Pie.
 */
public class Pie extends DecoratorCake {
    /**
     * Instantiates a new Pie.
     */
    public Pie() {
        super("Pie");
        description = "Pie";
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public double cost() {
        return 4.0;
    }
}
