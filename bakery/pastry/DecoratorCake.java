package bakery.pastry;

import bakery.Consumable;

import java.time.LocalDate;

/**
 * The type Decorator cake.
 */
public abstract class DecoratorCake extends Consumable {
    /**
     * The Description.
     */
    protected String description;

    /**
     * Instantiates a new Decorator cake.
     *
     * @param name the name
     */
    public DecoratorCake(String name) {
        this.name = name;
        this.expiryDate = LocalDate.now().plusDays(1);
    }

    /**
     * Instantiates a new Decorator cake.
     *
     * @param name        the name
     * @param description the description
     */
    public DecoratorCake(String name, String description) {
        this(name);
        this.description = description;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public abstract String getDescription();

    /**
     * Cost double.
     *
     * @return the double
     */
    public abstract double cost();

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets expiry date.
     *
     * @param expiryDate the expiry date
     */
    public void setExpiryDate(LocalDate expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getName() {
        return name;
    }


    public boolean isExpired() {
        return false;
    }
}
