package bakery.pastry;

import bakery.ingredient.Ingredient;

/**
 * The type Cake part.
 */
public class CakePart implements Ingredient {
    private String description;
    private double cost;

    /**
     * Instantiates a new Cake part.
     *
     * @param description the description
     * @param cost        the cost
     */
    public CakePart(String description, double cost) {
        this.description = description;
        this.cost = cost;
    }
    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public double cost() {
        return cost;
    }

    public String toString() {
        return description + ": " + cost() + "€";
    }
}
