package bakery.recipe;
import bakery.Recipe;

import java.util.HashMap;
import java.util.ArrayList;

import bakery.ingredient.Ingredient;
import bakery.pastry.CakePart;
import utils.Step;

/**
 * The type Apple pie.
 */
public class ApplePie extends Recipe{
    /**
     * Instantiates a new Apple pie.
     */
    public ApplePie(){
        super("Apple Pie");
        this.setIngredients();
        this.setSteps();
        this.createCake();
    }

    public void setIngredients(){
        this.ingredientList = new HashMap<Ingredient, Integer>();
        Ingredient pastry = new CakePart("Pastry", 0.2);
        Ingredient vanilla = new CakePart("Vanilla", 1);
        Ingredient butter = new CakePart("Butter", 0.05);
        Ingredient apple = new CakePart("Apple", 0.7);

        this.ingredientList.put(pastry, 1);
        this.ingredientList.put(vanilla, 1);
        this.ingredientList.put(butter, 30);
        this.ingredientList.put(apple, 6);
    }

    public void setSteps(){
        this.steps = new ArrayList<Step>();

        HashMap<Ingredient, Integer> step1List = new HashMap<Ingredient, Integer>();
        step1List.put(this.getIngredient("Apple"), 4);
        Step step1 = new Step("Cut apple", 5, step1List);
        this.steps.add(step1);

        HashMap<Ingredient, Integer> step2List = new HashMap<Ingredient, Integer>();
        step2List.put(this.getIngredient("Vanilla"), 1);
        Step step2 = new Step("Doing compote", 5, step2List);
        this.steps.add(step2);
        
        HashMap<Ingredient, Integer> step3List = new HashMap<Ingredient, Integer>();
        step3List.put(this.getIngredient("Apple"), 2);
        Step step3 = new Step("Cut apple again", 2, step3List);
        this.steps.add(step3);
        
        HashMap<Ingredient, Integer> step4List = new HashMap<Ingredient, Integer>();
        step4List.put(this.getIngredient("Pastry"), 1);
        Step step4 = new Step("Fork pastry", 1, step4List);
        this.steps.add(step4);

        HashMap<Ingredient, Integer> step5List = new HashMap<Ingredient, Integer>();
        step4List.put(this.getIngredient("Butter"), 30);
        Step step5 = new Step("Prepare plate and cook the pie", 30, step5List);
        this.steps.add(step5);
    }
}