package bakery.ingredient;

import bakery.pastry.DecoratorCake;
import designpattern.decorator.DecoratorCakeDecorator;

/**
 * The type Vanilla.
 */
public class Vanilla extends DecoratorCakeDecorator {
    /**
     * Instantiates a new Vanilla.
     *
     * @param decoratorCake the decorator cake
     */
    public Vanilla(DecoratorCake decoratorCake) {
        super(decoratorCake, "Vanilla");
        description = "Vanilla";
    }

    @Override
    public String getDescription() {
        return this.decoratorCake.getDescription() + ", " + this.description;
    }

    @Override
    public double cost() {
        return this.decoratorCake.cost() + 1.5;
    }
}

