package bakery.ingredient;

import bakery.pastry.DecoratorCake;
import designpattern.decorator.DecoratorCakeDecorator;

/**
 * The type Roasted almonds.
 */
public class RoastedAlmonds extends DecoratorCakeDecorator {
    /**
     * Instantiates a new Roasted almonds.
     *
     * @param decoratorCake the decorator cake
     */
    public RoastedAlmonds(DecoratorCake decoratorCake) {
        super(decoratorCake, "RoastedAlmonds");
    }

    public String getName() {
        return this.name;
    }

    @Override
    public String getDescription() {
        return this.decoratorCake.getDescription() + ", roasted almonds";
    }

    @Override
    public double cost() {
        return this.decoratorCake.cost() + 1.0;
    }
}
