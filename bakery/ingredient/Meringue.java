package bakery.ingredient;

import bakery.pastry.DecoratorCake;
import designpattern.decorator.DecoratorCakeDecorator;

/**
 * The type Meringue.
 */
public class Meringue extends DecoratorCakeDecorator {
    /**
     * Instantiates a new Meringue.
     *
     * @param decoratorCake the decorator cake
     */
    public Meringue(DecoratorCake decoratorCake) {
        super(decoratorCake, "Meringue");
    }

    @Override
    public String getDescription() {
        return this.decoratorCake.getDescription() + ", meringue";
    }

    @Override
    public double cost() {
        return this.decoratorCake.cost() + 0.80;
    }
}
