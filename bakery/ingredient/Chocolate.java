package bakery.ingredient;

import bakery.pastry.DecoratorCake;
import designpattern.decorator.DecoratorCakeDecorator;

/**
 * The type Chocolate.
 */
public class Chocolate extends DecoratorCakeDecorator {
    /**
     * Instantiates a new Chocolate.
     *
     * @param decoratorCake the decorator cake
     */
    public Chocolate(DecoratorCake decoratorCake) {
        super(decoratorCake, "Chocolate");
        description = "Chocolate";
    }

    @Override
    public String getDescription() {
        return this.decoratorCake.getDescription() + ", " + this.description;
    }

    @Override
    public double cost() {
        return this.decoratorCake.cost() + 1.0;
    }
}
