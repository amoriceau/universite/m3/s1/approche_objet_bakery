package bakery.ingredient;

/**
 * The interface Ingredient.
 */
public interface Ingredient {
    /**
     * Gets description.
     *
     * @return the description
     */
    String getDescription();

    /**
     * Cost double.
     *
     * @return the double
     */
    double cost();
}
