package bakery.ingredient;

import bakery.pastry.DecoratorCake;
import designpattern.decorator.DecoratorCakeDecorator;

/**
 * The type Hazelnuts.
 */
public class Hazelnuts extends DecoratorCakeDecorator {

    /**
     * Instantiates a new Hazelnuts.
     *
     * @param decoratorCake the decorator cake
     */
    public Hazelnuts(DecoratorCake decoratorCake) {
        super(decoratorCake, "Hazelnuts");
    }

    @Override
    public String getDescription() {
        return this.decoratorCake.getDescription() + ", hazelnuts";
    }

    @Override
    public double cost() {
        return this.decoratorCake.cost() + 0.75;
    }
}
