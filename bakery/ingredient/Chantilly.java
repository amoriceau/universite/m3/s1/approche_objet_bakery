package bakery.ingredient;

import bakery.pastry.DecoratorCake;
import designpattern.decorator.DecoratorCakeDecorator;

/**
 * The type Chantilly.
 */
public class Chantilly extends DecoratorCakeDecorator {

    /**
     * Instantiates a new Chantilly.
     *
     * @param decoratorCake the decorator cake
     */
    public Chantilly(DecoratorCake decoratorCake) {
        super(decoratorCake, "Chantilly");
    }

    @Override
    public String getDescription() {
        return this.decoratorCake.getDescription() + ", chantilly";
    }

    @Override
    public double cost() {
        return this.decoratorCake.cost() + 0.5;
    }
}
