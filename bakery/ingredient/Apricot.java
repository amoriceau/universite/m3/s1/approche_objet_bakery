package bakery.ingredient;

import bakery.pastry.DecoratorCake;
import designpattern.decorator.DecoratorCakeDecorator;

/**
 * The type Apricot.
 */
public class Apricot extends DecoratorCakeDecorator {
    /**
     * Instantiates a new Apricot.
     *
     * @param decoratorCake the decorator cake
     */
    public Apricot(DecoratorCake decoratorCake) {
        super(decoratorCake, "Apricot");
    }

    @Override
    public String getDescription() {
        return this.decoratorCake.getDescription() + ", apricot";
    }

    @Override
    public double cost() {
        return this.decoratorCake.cost() + 1.25;
    }
}
