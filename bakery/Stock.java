package bakery;

import designpattern.observer.Observer;

import java.util.ArrayList;

/**
 * The type Stock.
 */
public class Stock {
    /**
     * The Entries.
     */
    ArrayList<StockEntry> entries;
    ArrayList<Observer> entriesObservers;

    /**
     * Instantiates a new Stock.
     *
     * @param entries the entries
     */
    public Stock(ArrayList<StockEntry> entries) {
        this.entries = entries;
        this.entriesObservers = new ArrayList<>();
    }

    /**
     * Instantiates a new Stock.
     */
    public Stock() {
        this.entries = new ArrayList<>();
        this.entriesObservers = new ArrayList<>();
    }

    /**
     * Gets entries.
     *
     * @return the entries
     */
    public ArrayList<StockEntry> getEntries() {
        return entries;
    }

    /**
     * Sets entries.
     *
     * @param entries  the entries
     * @param observer the observer
     */
    public void setEntries(ArrayList<StockEntry> entries, Observer observer) {
        this.entries = entries;
        this.entriesObservers.add(observer);
        for (StockEntry entry: entries) {
            for(Observer obs: entriesObservers){
                entry.registerObserver(obs);
            }
        }
    }

    /**
     * Sets entries.
     *
     * @param entries the entries
     */
    public void setEntries(ArrayList<StockEntry> entries) {
        this.entries = entries;
    }
    
    public void addEntry(StockEntry entry){
        this.entries.add(entry);
        for(Observer obs: entriesObservers){
            entry.registerObserver(obs);
        }
    }

    public void removeEntry(StockEntry entry){
        this.entries.remove(entry);
        for(Observer obs: entriesObservers){
            entry.removeObserver(obs);
        }
    }

    public StockEntry getEntry(String name){
        for (StockEntry entry: this.getEntries()) {
            if (entry.getEntry().getDescription().equals(name)){
                return entry;
            }
        }
        
        return null;
    }

}
