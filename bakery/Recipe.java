package bakery;
import designpattern.builder.Builder;

import java.util.List;
import java.util.Map;
import designpattern.composite.CompositeCake;
import bakery.ingredient.Ingredient;
import utils.Step;

/**
 * The type Recipe.
 */
public abstract class Recipe implements Builder{
    /**
     * The Cake.
     */
    protected CompositeCake cake;
    /**
     * The Ingredient list.
     */
    protected Map<Ingredient, Integer> ingredientList;
    /**
     * The Steps.
     */
    protected List<Step> steps;

    /**
     * Instantiates a new Recipe.
     *
     * @param newName the new name
     */
    public Recipe(String newName){
        this.cake = new CompositeCake(newName);
    };

    public void createCake(){
        for (Step step : this.steps) {
            step.proceedStep(this.cake);
        }
    };

    private void consumeIngredients(Step step){
        Map<Ingredient, Integer> stepIngredientList = step.getIngredientList();
        for (Ingredient ing : stepIngredientList.keySet()) {
            Integer ingredientQuantity = stepIngredientList.get(ing);
            this.ingredientList.put(ing, this.ingredientList.get(ing) - ingredientQuantity);
        }
    }

    /**
     * Get ingredient ingredient.
     *
     * @param ingredientName the ingredient name
     * @return the ingredient
     */
    protected Ingredient getIngredient(String ingredientName){
        for (Ingredient ingredient: ingredientList.keySet()) {
            if (ingredient.getDescription().equals(ingredientName)){
                return ingredient;
            }
        }
        return null;
    }
    public abstract void setIngredients();
    public abstract void setSteps();

    /**
     * Gets cake.
     *
     * @return the cake
     */
    public CompositeCake getCake() {
        return cake;
    }

    /**
     * Sets cake.
     *
     * @param cake the cake
     */
    public void setCake(CompositeCake cake) {
        this.cake = cake;
    }

    @Override
    public String toString() {
        StringBuilder sbIngredient = new StringBuilder();
        for (Ingredient ingredient: this.ingredientList.keySet()) {
            sbIngredient.append(ingredient.getDescription() + "=" + this.ingredientList.get(ingredient)+"; ");
        }

        StringBuilder sbSteps = new StringBuilder();
        for (Step step: this.steps) {
            sbSteps.append(step.getDescription() + "=" + step.getTime()+"min; ");
        }


        return "{" +
                "\n cake=" + cake.getDescription() +
                ",\n ingredientList=" + sbIngredient +
                ",\n steps=" + sbSteps +
                "\n}";
    }
}