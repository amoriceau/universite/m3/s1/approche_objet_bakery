package bakery;

import java.time.LocalDate;

/**
 * The type Consumable.
 */
public abstract class Consumable {
    /**
     * The Name.
     */
    protected String name;
    /**
     * The Expiry date.
     */
    protected LocalDate expiryDate;

    /**
     * Gets name.
     *
     * @return the name
     */
    protected abstract String getName();

    /**
     * Get expiry date local date.
     *
     * @return the local date
     */
    protected LocalDate getExpiryDate(){
            return expiryDate;
    }

    /**
     * Is expired boolean.
     *
     * @return the boolean
     */
    protected boolean isExpired(){
        return this.expiryDate.isAfter(LocalDate.now());
    }
}
