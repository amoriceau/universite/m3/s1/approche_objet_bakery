package bakery;

import designpattern.composite.CompositeCake;

import java.util.ArrayList;

/**
 * The type Catalog.
 * Used to knows what's considered as a "cake" or at least sellable consumable in the bakery,
 * This allows us to create Ingredient like Flour that would lie in the stock yet not be for sale to a customer
 * (See bakery.Bakery@sell)
 */
public class Catalog {
    /**
     * The Pastries.
     */
    ArrayList<CompositeCake> pastries;

    /**
     * Instantiates a new Catalog.
     *
     * @param pastries the pastries
     */
    public Catalog(ArrayList<CompositeCake> pastries) {
        this.pastries = pastries;
    }

    /**
     * Instantiates a new Catalog.
     */
    public Catalog() {
        this.pastries = new ArrayList<>();
    }

    /**
     * Gets pastries.
     *
     * @return the pastries
     */
    public ArrayList<CompositeCake> getPastries() {
        return pastries;
    }

    /**
     * Sets pastries.
     *
     * @param pastries the pastries
     */
    public void setPastries(ArrayList<CompositeCake> pastries) {
        this.pastries = pastries;
    }

    @Override
    public String toString() {
        return "Catalog{" +
                "pastries=" + pastries +
                '}';
    }
}
