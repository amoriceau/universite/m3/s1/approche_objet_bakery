package bakery;

import bakery.ingredient.Ingredient;
import designpattern.observer.Observable;
import designpattern.observer.Observer;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Stock entry.
 */
public class StockEntry implements Observable {
    /**
     * The Quantity.
     */
    int quantity;
    /**
     * The Entry.
     */
    Ingredient entry;
    /**
     * The Minimum availability.
     */
    final int minimumAvailability;
    private List<Observer> observers;


    /**
     * Instantiates a new Stock entry.
     *
     * @param quantity            the quantity
     * @param entry               the entry
     * @param minimumAvailability the minimum availability
     */
    public StockEntry(int quantity, Ingredient entry, int minimumAvailability) {
        this.quantity = quantity;
        this.entry = entry;
        this.minimumAvailability = minimumAvailability;
        this.observers = new ArrayList<>();
    }

    /**
     * Instantiates a new Stock entry.
     *
     * @param quantity the quantity
     * @param entry    the entry
     */
    public StockEntry(int quantity, Ingredient entry) {
        this(quantity, entry, 3);
    }

    /**
     * Gets quantity.
     *
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Sets quantity.
     *
     * @param quantity the quantity
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
        this.notifyObservers();
    }

    /**
     * Gets entry.
     *
     * @return the entry
     */
    public Ingredient getEntry() {
        return entry;
    }

    /**
     * Sets entry.
     *
     * @param entry the entry
     */
    public void setEntry(Ingredient entry) {
        this.entry = entry;
    }

    /**
     * Gets minimum availability.
     *
     * @return the minimum availability
     */
    public int getMinimumAvailability() {
        return minimumAvailability;
    }

    @Override
    public void registerObserver(Observer observer) {
        this.observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        this.observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : this.observers) {
            observer.update(this);
        }
    }
}
