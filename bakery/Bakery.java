package bakery;

import designpattern.composite.CompositeCake;
import designpattern.factory.Chef;
import utils.Address;
import utils.SellingHistory;
import utils.SellingHistoryEntry;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * The type Bakery.
 */
public class Bakery {
    /**
     * The Name.
     */
    String name;
    /**
     * The Address.
     */
    Address address;
    /**
     * The Stock.
     */
    Stock stock;
    /**
     * The Catalog.
     */
    Catalog catalog;
    /**
     * The Chefs.
     */
    ArrayList<Chef> chefs;

    SellingHistory sellingHistory;

    /**
     * Instantiates a new Bakery.
     *
     * @param name    the name
     * @param address the address
     * @param stock   the stock
     * @param catalog the catalog
     * @param chefs   the chefs
     */
    public Bakery(String name, Address address, Stock stock, Catalog catalog, ArrayList<Chef> chefs) {
        this.name = name;
        this.address = address;
        this.stock = stock;
        this.catalog = catalog;
        this.chefs = chefs;
        this.sellingHistory = new SellingHistory();
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets address.
     *
     * @return the address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Gets stock.
     *
     * @return the stock
     */
    public Stock getStock() {
        return stock;
    }

    /**
     * Gets catalog.
     *
     * @return the catalog
     */
    public Catalog getCatalog() {
        return catalog;
    }

    /**
     * Gets pastry cooks.
     *
     * @return the pastry cooks
     */
    public ArrayList<Chef> getPastryCooks() {
        return chefs;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets address.
     *
     * @param address the address
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * Sets stock.
     *
     * @param stock the stock
     */
    public void setStock(Stock stock) {
        this.stock = stock;
    }

    /**
     * Sets catalog.
     *
     * @param catalog the catalog
     */
    public void setCatalog(Catalog catalog) {
        this.catalog = catalog;
    }

    /**
     * Sets pastry cooks.
     *
     * @param pastryCooks the pastry cooks
     */
    public void setPastryCooks(ArrayList<Chef> pastryCooks) {
        this.chefs = pastryCooks;
    }

    /**
     * Sell.
     *
     * @param pastryName the pastry name
     * @param quantity   the quantity
     */
    public void sell(String pastryName, int quantity){
        CompositeCake cake = null;
        for (CompositeCake pastry: this.catalog.getPastries()) {
            if(pastryName.equals(pastry.getDescription())){
                cake = pastry;
                break;
            }
        }

        StockEntry entry = this.stock.getEntry(pastryName);

        if (entry == null || cake == null) {
                System.out.println("This pastry is not available.");
                return;
        }

        if (entry.getQuantity() < quantity){
            System.out.println("We don't have enough of this pastry to fulfill you order.");
                return;
            }

        DecimalFormat df = new DecimalFormat("0.00");

        double price = entry.getEntry().cost() * quantity;
        
        System.out.println("[Cashier] " + df.format(price) + "€ please.");
        entry.setQuantity(entry.getQuantity() - quantity);

        StringBuilder transaction = new StringBuilder("[");
        transaction.append(LocalDateTime.now().toString());
        transaction.append("] ");
        transaction.append(quantity + " ");
        transaction.append(entry.getEntry().getDescription() + " (" + entry.getEntry().cost() + ")");

        this.sellingHistory.addEntry(new SellingHistoryEntry(quantity + " " + pastryName, price));
        return;
            
    }

    @Override
    public String toString() {
        return "\nBakery: " + this.name + "\n" +
                "Located at: " + address + "\n\n" + 
                this.sellingHistory.toString();
    }
}
