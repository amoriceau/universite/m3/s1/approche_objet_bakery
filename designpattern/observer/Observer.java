package designpattern.observer;

import bakery.StockEntry;

/**
 * The interface Observer.
 */
public interface Observer {
    /**
     * Update.
     *
     * @param entry the entry
     */
    void update(StockEntry entry);
}
