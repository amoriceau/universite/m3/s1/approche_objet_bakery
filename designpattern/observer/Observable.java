package designpattern.observer;

/**
 * The interface Observable.
 */
public interface Observable {
    /**
     * Register observer.
     *
     * @param observer the observer
     */
    void registerObserver(Observer observer);

    /**
     * Remove observer.
     *
     * @param observer the observer
     */
    void removeObserver(Observer observer);

    /**
     * Notify observers.
     */
    void notifyObservers();
}
