package designpattern.decorator;

import bakery.pastry.DecoratorCake;

/**
 * The type Decorator cake decorator.
 */
public abstract class DecoratorCakeDecorator extends DecoratorCake {

    /**
     * The Decorator cake.
     */
    protected DecoratorCake decoratorCake;

    /**
     * Instantiates a new Decorator cake decorator.
     *
     * @param decoratorCake the decorator cake
     * @param name          the name
     */
    public DecoratorCakeDecorator(DecoratorCake decoratorCake, String name) {
        super(name);
        this.decoratorCake = decoratorCake;
    }

    /**
     * Instantiates a new Decorator cake decorator.
     *
     * @param decoratorCake the decorator cake
     * @param name          the name
     * @param description   the description
     */
    public DecoratorCakeDecorator(DecoratorCake decoratorCake, String name, String description) {
        super(name, description);
        this.decoratorCake = decoratorCake;
    }

    @Override
    public abstract String getDescription();

    @Override
    public abstract double cost();

    /**
     * Gets base cake description.
     *
     * @return the base cake description
     */
    public String getBaseCakeDescription() {
        return decoratorCake.getDescription();
    }

    /**
     * Gets base cake cost.
     *
     * @return the base cake cost
     */
    public double getBaseCakeCost() {
        return decoratorCake.cost();
    }

    @Override
    public void setName(String name) {
        decoratorCake.setName(name);
    }

    @Override
    public String getName() {
        return decoratorCake.getName();
    }

    @Override
    public boolean isExpired() {
        return decoratorCake.isExpired();
    }
}
