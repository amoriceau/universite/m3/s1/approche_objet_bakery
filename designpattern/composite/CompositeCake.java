package designpattern.composite;

import bakery.ingredient.Ingredient;
import bakery.pastry.CakePart;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Composite cake.
 */
public class CompositeCake implements Ingredient {
    private List<Ingredient> parts = new ArrayList<>();
    private String description;

    /**
     * Instantiates a new Composite cake.
     *
     * @param description the description
     */
    public CompositeCake(String description) {
        this.description = description;
    }

    /**
     * Add part.
     *
     * @param part the part
     */
    public void addPart(Ingredient part) {
        this.parts.add(part);
    }

    /**
     * Remove part.
     *
     * @param part the part
     */
    public void removePart(CakePart part){
        this.parts.remove(part);
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public double cost() {
        double totalCost = 0.0;
        for (Ingredient part : parts) {
            totalCost += part.cost();
        }
        return totalCost;
    }

    /**
     * Gets parts.
     *
     * @return the parts
     */
    public List<Ingredient> getParts() {
        return parts;
    }

    @Override
    public String toString() {
        return description + ": " + cost() + "€";
    }
}
