package designpattern.factory;

import bakery.Stock;
import bakery.StockEntry;
import bakery.pastry.CakePart;
import bakery.recipe.ApplePie;
import designpattern.composite.CompositeCake;
import designpattern.observer.Observer;

/**
 * The type Chef.
 */
public class Chef implements Observer {
    /**
     * The Bakery stock.
     */
    Stock bakeryStock;

    /**
     * Instantiates a new Chef.
     *
     * @param stock the stock
     */
    public Chef(Stock stock){
        this.bakeryStock = stock;
        for (StockEntry entry: stock.getEntries()) {
            entry.registerObserver(this);
        }
    }

    /**
     * Bake factory apple pie composite cake.
     *
     * @return the composite cake
     */
    public static CompositeCake bakeFactoryApplePie(){
        CompositeCake applePie = new CompositeCake("Apple pie");
        applePie.addPart(new CakePart("Pastry", 0.2));
        applePie.addPart(new CakePart("Vanilla", 1));
        applePie.addPart(new CakePart("Butter", 0.05));
        applePie.addPart(new CakePart("Apple", 0.7));

        return applePie;
    }

    /**
     * Bake builder apple pie composite cake.
     *
     * @return the composite cake
     */
    public static CompositeCake bakeBuilderApplePie(){
        return new ApplePie().getCake();
    }

    @Override
    public void update(StockEntry entry) {
        if(entry.getQuantity() < entry.getMinimumAvailability()){
            System.out.println("[Observer][update] Quantity to low -> "+ entry.getQuantity() + " " +  entry.getEntry().getDescription() +" remaining. Baking " + entry.getMinimumAvailability() + " " + entry.getEntry().getDescription() + ".\n");
            entry.setQuantity(entry.getQuantity() + entry.getMinimumAvailability());
        }
    }
}
