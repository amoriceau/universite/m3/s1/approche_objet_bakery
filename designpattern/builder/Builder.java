package designpattern.builder;

/**
 * The interface Builder.
 */
public interface Builder {
    /**
     * Create cake.
     */
    void createCake();

    /**
     * Sets ingredients.
     */
    void setIngredients();

    /**
     * Sets steps.
     */
    void setSteps();
}