import bakery.Bakery;
import bakery.Catalog;
import bakery.Stock;
import bakery.StockEntry;
import bakery.pastry.CakePart;
import bakery.recipe.ApplePie;
import designpattern.composite.CompositeCake;
import designpattern.factory.Chef;
import utils.Address;
import utils.menus.BakeryMenu;

import java.util.ArrayList;

/**
 * The type Main.
 */
public class Main {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        Address bakeryAddress = new Address("12 place de la Concorde", "Paris", "Île-de-France", "75001", "France");
        Stock bakeryStock = new Stock();

        Catalog bakeryCatalog = new Catalog();

        Chef chef = new Chef(bakeryStock);
        ArrayList<Chef> bakeryChefs = new ArrayList<>();
        bakeryChefs.add(chef);

        Bakery bakery = new Bakery("Boulangerie", bakeryAddress, bakeryStock,bakeryCatalog, bakeryChefs);

        // System.out.println(bakery);

        // create some pastries
        CompositeCake applePie = new ApplePie().getCake();

        CompositeCake creamPuff = new CompositeCake("Cream puff");
        creamPuff.addPart(new CakePart("Puff", 0.25));
        creamPuff.addPart(new CakePart("Cream", 0.35));

        CompositeCake strawberryMeringue = new CompositeCake("Strawberry flavoured Meringue");
        strawberryMeringue.addPart(new CakePart("Eggs", 0.15));
        strawberryMeringue.addPart(new CakePart("Strawberry", 0.6));

        // System.out.println(applePie);
        // System.out.println(creamPuff);
        // System.out.println(strawberryMeringue);

        ArrayList<CompositeCake> pastries = new ArrayList<>();
        pastries.add(applePie);
        pastries.add(creamPuff);
        pastries.add(strawberryMeringue);

        bakeryCatalog.setPastries(pastries);

        // bakery.sell("Apple Pie", 3);

        ArrayList<StockEntry> bakeryStockEntries = new ArrayList<>();
        bakeryStockEntries.add(new StockEntry(4, applePie, 5));
        bakeryStockEntries.add(new StockEntry(100, creamPuff, 10));
        bakeryStockEntries.add(new StockEntry(7, strawberryMeringue));

        bakeryStock.setEntries(bakeryStockEntries, chef);

        // bakery.sell("Apple Pie", 3);

        BakeryMenu.welcomeMenu(bakery);
    }
}