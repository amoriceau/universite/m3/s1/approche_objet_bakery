package utils;

import java.text.DecimalFormat;

public class SellingHistoryEntry {
    public String name;
    public double value;

    public SellingHistoryEntry(String name, double value){
        this.name = name;
        this.value = value;
    }

    @Override 
    public String toString(){
        return name + " - " + new DecimalFormat("0.00").format(value) + "€";
    }
}
