package utils;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class SellingHistory {
    private Map<LocalDateTime,SellingHistoryEntry> history;

    public SellingHistory(){
        this.history = new HashMap<>();
    }

    public Map<LocalDateTime,SellingHistoryEntry> getHistory(){
        return this.history;
    }

    public void addEntry(SellingHistoryEntry entry){
        this.history.put(LocalDateTime.now(), entry);
    }

    public double income(){
        double total = 0.0;
        for (SellingHistoryEntry entry : this.getHistory().values()) {
            total += entry.value;
        }

        return total;
    }
    

    @Override
    public String toString(){
        StringBuilder historyBuilder = new StringBuilder("Selling History for " + LocalDate.now().toString() +":\n");
        int count = 0;
        for (LocalDateTime transaction : this.history.keySet()) {
            historyBuilder.append("- [" + transaction + "] " + this.history.get(transaction) + "\n");
            count++;
        }
        
        String income = new DecimalFormat("0.00").format(this.income()) + "€";

        historyBuilder.append("Total income at " + LocalDateTime.now().toString() +": " + income + "\n");
        historyBuilder.append(count + " transactions registered.\n");
        

        return historyBuilder.toString();
    }
}
