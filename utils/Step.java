package utils;

import java.util.Map;
import designpattern.composite.CompositeCake;
import bakery.ingredient.Ingredient;

/**
 * The type Step.
 */
public class Step{
    private String description;
    private double time;
    private Map<Ingredient, Integer> ingredientList;

    /**
     * Instantiates a new Step.
     *
     * @param newDescription    the new description
     * @param newTime           the new time
     * @param newIngredientList the new ingredient list
     */
    public Step(String newDescription, double newTime, Map<Ingredient, Integer> newIngredientList){
        this.description = newDescription;
        this.time = newTime;
        this.ingredientList = newIngredientList;
    }

    /**
     * Get ingredient list map.
     *
     * @return the map
     */
    public Map<Ingredient, Integer> getIngredientList(){
        return this.ingredientList;
    }

    /**
     * Proceed step.
     *
     * @param cake the cake
     */
    public void proceedStep(CompositeCake cake){
        for (Ingredient ing : this.ingredientList.keySet()) {
            cake.addPart(ing);
        }
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets time.
     *
     * @return the time
     */
    public double getTime() {
        return time;
    }

    @Override
    public String toString() {
        return "Step{" +
                "description='" + description + '\'' +
                ", time=" + time +
                ", ingredientList=" + ingredientList +
                '}';
    }
}