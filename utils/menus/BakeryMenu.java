package utils.menus;

import bakery.Bakery;
import bakery.StockEntry;

import java.util.Scanner;

public class BakeryMenu {

    public static void welcomeMenu(Bakery bakery){
            while (true) {
            int choice;
            Scanner input = new Scanner(System.in);

            try {
                System.out.println("What do you want to do");
                System.out.println("-------------------------\n");
                System.out.println("1 - Buy some cake");
                System.out.println("2 - Get bakery details");
                System.out.println("3 - Quit");

                choice = input.nextInt();

            } catch (Exception exception) {
                input.close();
                throw exception;
            }

            switch (choice) {
                case 1:
                    catalogMenu(bakery, input);
                    break;
                case 2:
                    System.out.println(bakery.toString() + "\n");
                    break;
                case 3:
                    System.out.println("Goodbye then.");
                    input.close();
                    return;
                default:
                    System.out.println("You can't do that.\n\n");
                    break;
            }
        }
    }

    public static void catalogMenu(Bakery bakery, Scanner input){
            String choice;   
            int quantity;
            try {
                    System.out.println("\nHere are our available entries, please tell me what you want to buy: (Enter the complete name or 'Q' to quit)");
                    System.out.println("-------------------------\n");
                    for(StockEntry entry: bakery.getStock().getEntries()){
                        System.out.println("- " + entry.getEntry() + " (" + entry.getQuantity() + " remaining)");
                    }
                    input.nextLine();
                    choice = input.nextLine();
                    if(choice.toLowerCase().equals("q")){
                        return;
                    }

                    System.out.println("\nAnd how much " + choice + " do you want ?\n");
                    quantity = input.nextInt();

                    System.out.println("Selling " + quantity + " " + choice);
                    bakery.sell(choice, quantity);
            } catch (Exception exception) {
                    input.close();
                    throw exception;
            }
    }
}
